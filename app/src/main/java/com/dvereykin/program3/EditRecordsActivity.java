package com.dvereykin.program3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.dvereykin.program3.PersonalDataIntent.ActionType;


public class EditRecordsActivity extends AppCompatActivity implements
        View.OnClickListener, ListView.OnItemClickListener, ListView.OnItemSelectedListener {

    Button addButton;
    Button deleteButton;
    Button editButton;
    Button viewButton;

    ListView listAddresses;

    AddressArrayAdapter addressAdapter;
    PersonalDataCollection addresses = new PersonalDataCollection();

    final int ADDRESS_ENTRY = 1001;
    int recordNumber = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_records);

        addButton = (Button) findViewById(R.id.add_button);
        deleteButton = (Button) findViewById(R.id.delete_button);
        editButton = (Button) findViewById(R.id.edit_button);
        viewButton = (Button) findViewById(R.id.view_button);

        addButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);
        editButton.setOnClickListener(this);
        viewButton.setOnClickListener(this);

        listAddresses = (ListView) findViewById(R.id.list_addresses);
        listAddresses.setOnItemClickListener(this);
        listAddresses.setOnItemSelectedListener(this);
        addressAdapter = new AddressArrayAdapter(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                addresses);
        listAddresses.setAdapter(addressAdapter);
    }

    public void onClick(View v) {
        PersonalDataIntent pdIntent;

        if (addButton.getId() == v.getId()) {
            pdIntent = new PersonalDataIntent();
            pdIntent.action = ActionType.ADD;
            startActivityForResult(pdIntent.getIntent(this, EditValuesActivity.class), ADDRESS_ENTRY);
        } else {
            try {

                AddressAttributeGroup address = addresses.getAddress(recordNumber);

                if (editButton.getId() == v.getId()) {
                    pdIntent = new PersonalDataIntent(address, ActionType.EDIT, recordNumber);
                    startActivityForResult(pdIntent.getIntent(this, EditValuesActivity.class), ADDRESS_ENTRY);
                }

                if (deleteButton.getId() == v.getId()) {
                    pdIntent = new PersonalDataIntent(address, ActionType.DELETE, recordNumber);
                    startActivityForResult(pdIntent.getIntent(this, EditValuesActivity.class), ADDRESS_ENTRY);
                }

                if (viewButton.getId() == v.getId()) {
                    pdIntent = new PersonalDataIntent(address, ActionType.VIEW, recordNumber);
                    startActivityForResult(pdIntent.getIntent(this, EditValuesActivity.class), ADDRESS_ENTRY);
                }
            } catch (Exception ex) {
                showMessage(ex.getMessage());
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        PersonalDataIntent personalDataIntent = new PersonalDataIntent(data);

        if (requestCode == ADDRESS_ENTRY) {
            try {
                switch (resultCode) {
                    case RESULT_OK:
                        AddressAttributeGroup address = new AddressAttributeGroup(personalDataIntent.name,
                                personalDataIntent.address, personalDataIntent.city, personalDataIntent.state, personalDataIntent.zipCode);

                        switch (personalDataIntent.action) {
                            case ADD:
                                addresses.addAddress(address);
                                break;
                            case DELETE:
                                addresses.removeAddress(personalDataIntent.addressIndex);
                                showMessage("Deleted");
                                recordNumber = -1;
                                break;
                            case EDIT:
                                addresses.setAddress(personalDataIntent.addressIndex, address);
                                showMessage("Updated");
                                break;
                        }

                        addressAdapter.notifyDataSetChanged();
                        break;

                    case RESULT_CANCELED:
                        showMessage("Cancelled");
                        break;
                }
            } catch (Exception ex) {
                showMessage(ex.getMessage());
            }
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onItemSelected(parent, view, position, id);
    }

    public void onItemSelected(AdapterView<?>parent, View view, int position, long id) {
        recordNumber = position;
    }

    public void onNothingSelected(AdapterView<?> parent) {
    }

    void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}

class AddressArrayAdapter extends ArrayAdapter<AddressAttributeGroup> {
    private final Context context;
    private final PersonalDataCollection addresses;

    AddressArrayAdapter(Context context, int resource,
                               int textViewResourceId, PersonalDataCollection addresses) {
        super(context, resource, textViewResourceId, addresses.addressList);
        this.context = context;
        this.addresses = addresses;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AddressAttributeGroup address = addresses.getAddress(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.layout_address_row, parent, false);
        TextView nameTextView = (TextView) rowView.findViewById(R.id.full_name);
        TextView addressTextView = (TextView) rowView.findViewById(R.id.full_address);
        nameTextView.setText(address.name);
        addressTextView.setText(address.address + " " + address.city +
                            " " + address.state + " " + address.zipCode);

        return rowView;
    }


}
