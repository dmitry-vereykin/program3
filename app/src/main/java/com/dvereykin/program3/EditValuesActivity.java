package com.dvereykin.program3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.dvereykin.program3.PersonalDataIntent.ActionType;


public class EditValuesActivity extends AppCompatActivity implements View.OnClickListener {
    Button saveButton;
    Button clearButton;
    Button cancelButton;

    EditText editName;
    EditText editAddress;
    EditText editCity;
    EditText editState;
    EditText editZipCode;

    String data = "";
    int result;

    PersonalDataIntent pdIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_values);

        saveButton = (Button) findViewById(R.id.save_button);
        clearButton = (Button) findViewById(R.id.clear_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);

        editName = (EditText) findViewById(R.id.editNameFld);
        editAddress = (EditText) findViewById(R.id.editAddressFld);
        editCity = (EditText) findViewById(R.id.editCityFld);
        editState = (EditText) findViewById(R.id.editStateFld);
        editZipCode = (EditText) findViewById(R.id.editZipCodeFld);

        saveButton.setOnClickListener(this);
        clearButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        pdIntent = new PersonalDataIntent(getIntent());

        editName.setText(pdIntent.name);
        editAddress.setText(pdIntent.address);
        editCity.setText(pdIntent.city);
        editState.setText(pdIntent.state);
        editZipCode.setText(pdIntent.zipCode);

        if (pdIntent.action == ActionType.DELETE) {
            saveButton.setText(R.string.delete_button_str);
            editName.setEnabled(false);
            editAddress.setEnabled(false);
            editCity.setEnabled(false);
            editState.setEnabled(false);
            editZipCode.setEnabled(false);
            clearButton.setEnabled(false);
        }

        if (pdIntent.action == ActionType.VIEW) {
            saveButton.setText(R.string.ok_button_str);
            editName.setEnabled(false);
            editAddress.setEnabled(false);
            editCity.setEnabled(false);
            editState.setEnabled(false);
            editZipCode.setEnabled(false);
            clearButton.setEnabled(false);
        }
    }

    public void onClick(View v) {
        if (saveButton.getId() == v.getId()) {

            if (editName.getText().toString().equals("")
                    || editAddress.getText().toString().equals("")
                    || editCity.getText().toString().equals("")
                    || editState.getText().toString().equals("")
                    || editZipCode.getText().toString().equals("")) {
                showMessage("Alert! One or more of the fields are empty");
            }

            else {

                data += editName.getText() + ", " + editAddress.getText() + ", "
                        + editCity.getText() + ", " + editState.getText() + ", "
                        + editZipCode.getText();

                if (saveButton.getText().equals("OK"))
                    showMessage("OK.");
                else
                    showMessage("Success! " + data + " saved.");

                result = RESULT_OK;
                finish();

            }
        }

        if (clearButton.getId() == v.getId()) {
            clear();
        }

        if (cancelButton.getId() == v.getId()) {
            result = RESULT_CANCELED;
            finish();
        }
    }

    void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void clear() {
        editName.setText("");
        editAddress.setText("");
        editState.setText("");
        editCity.setText("");
        editZipCode.setText("");
        data = "";
    }

    public void finish() {

        pdIntent.clearIntent();
        pdIntent.name = editName.getText().toString();
        pdIntent.address = editAddress.getText().toString();
        pdIntent.city = editCity.getText().toString();
        pdIntent.state = editState.getText().toString();
        pdIntent.zipCode = editZipCode.getText().toString();

        setResult(result, pdIntent.getIntent());
        super.finish();
    }
}

