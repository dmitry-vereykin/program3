package com.dvereykin.program3;

import java.util.ArrayList;

/**
 * Created by Dmitry Vereykin aka eXrump on 10/20/2016.
 */

class PersonalDataCollection {
    ArrayList<AddressAttributeGroup> addressList = new ArrayList<AddressAttributeGroup>();

    private boolean isAddressLimitReached() {
        int MAX_ADDRESS_COUNT = 15;
        return (addressList.size() >= MAX_ADDRESS_COUNT);
    }

    int addAddress(AddressAttributeGroup address) throws Exception {
        if (isAddressLimitReached())
            throw(new Exception("Max Address Reached."));

        addressList.add(address);
        return addressList.indexOf(address);
    }

    void setAddress(int addressIndex, AddressAttributeGroup address) {
        addressList.set(addressIndex,address);
    }

    void removeAddress(int addressIndex) {
        addressList.remove(addressIndex);
    }
        AddressAttributeGroup getAddress(int addressIndex) {
        return addressList.get(addressIndex);
    }
}

class AddressAttributeGroup {
    String name;
    String address;
    String city;
    String state;
    String zipCode;
    AddressAttributeGroup(String name, String address, String city, String state, String zipCode) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }
}
